var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var listproductSchema = new Schema({
  productName: String,
  productOwner: String,
  productCost:Number,
  shippingCost:Number,
  shippingAddress:String,
  phoneNumber:Number,
  status:String,
  date:Date,
});
//   module.exports=mongoose.model('productschema',listproductSchema) 
  const listmodel = mongoose.model('listproductSchema', listproductSchema) 
  module.exports=listmodel;
  