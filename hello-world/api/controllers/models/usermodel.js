var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  firstName:  String,
  lastName: String,
  emailId: String,
  password: String,
  role: String
  
});
const usermodel = mongoose.model('userSchema', userSchema)
// module.exports=mongoose.model('user',userSchema)
module.exports=usermodel;
