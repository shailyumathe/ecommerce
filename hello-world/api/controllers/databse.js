// 'use strict';
// var express = require('express');
var util = require('util');
// var router = express.Router();
const userModel = require('./models/usermodel');
const listmodel = require('./models/productmodel');
Response = require('../lib/res')
var jwt = require('jsonwebtoken');
// var mongoose=require('mongoose');
var mySecret = "shaily"
module.exports = {
    register: register,
    login: login,
    addproduct: addproduct,
    listproduct: listproduct,
    deleteproduct: deleteproduct,
    updateProduct: updateProduct
  }
  function register(req, res) {
  let user =  new userModel({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    emailId: req.body.emailId,
    password: req.body.password,
    role: "string"
   })
   user.save(user, function (err, data)
  {
    if(err)
    {
     res.json({
       code: 404,
       message: 'user not added'
     });
    }
    else
    {
      console.log("data is >>" ,data)
      res.json({
        "code": 200,
        "data": data
      });
      console.log('successfully created');
    }
  })
  }

  // router.post('/addLogin', function(req, res, next) {
  function login(req, res) {
    console.log("req.body>>>>>>>>>>",req.body)
    userModel.findOne({"emailId":req.body.emailId , "password":req.body.password}).exec(function(err ,data)
  {
    if(data)
    { console.log("enter????????????????????",data)
      var token = jwt.sign({ id: data._id }, mySecret, {
        expiresIn: 100 //expires in 100 secound
      })
      console.log(token ,">>>>>>" )
      res.json({status:200,token:token,data:data})
    }
    else
    {
      console.log("jwt token varification is not successful >>>>>>>>>" );
      res.json({status:400,token:null})
    }
   });
  }
  function addproduct(req, res) {
  // router.post('/addProduct', function(req, res, next) {
    console.log("req.body>>>>>>>>>>",req.body)
  
    let recordDetails =  new listmodel({
      productName: req.body.pname,
      productOwner: req.body.powner,
      productCost: req.body.pcost,
      shippingCost: req.body.scost,
      shippingAddress: req.body.add,
      phoneNumber: req.body.pnum,
      status: req.body.stat,
      date: req.body.date,
     
     })
   
     recordDetails.save(function(err, data)
    {
      if(err)
      {
        console.log("err is >>" ,err)
      }
      else
      {
        console.log("data is >>" ,data)
        res.json({"data" : "success"})
      }
     })
    // });
    }
    // router.get('/productList', function(req, res, next) {
      function listproduct(req, res) {
        console.log("req.body>>>>>>>>>>",req.body)
      var gArray=[{
        $project:{
          productName:'$productName',
          productOwner:'$productOwner',
          productCost:'$productCost',
          shippingCost:'$shippingCost',
          shippingAddress:'$shippingAddress',
          phoneNumber:'$phoneNumber',
          status:'$status',
          date:'$date'
        }
      }]
      listmodel.aggregate(gArray,function(err,data){
        res.json(data);
      })
    }

     function deleteproduct(req,res){
      // deleteproduct.delete("/deleteProduct/:id", (req,res)=>{
      console.log("inside delete", req.swagger.params.id.value)
      listmodel.findByIdAndRemove({_id: req.swagger.params.id.value}, function(err,data) {
        if(err) throw err;
        console.log("Deleted Successfully");
        data.save();
        res.json(data);
        })
      }
      function updateProduct(req,res){
        console.log("Updated Successfully");
      // router.put('/updateProduct',function(req,res,next){
        let productName=req.body.newProductName;
        let productCost=req.body.newProductCost;

        var updatedId={
          _id:req.body.id
        }

        listmodel.findByIdAndUpdate(updatedId,{$set:{productName:productName,productCost:productCost}},function(err,data){
        if(err) throw err;
        console.log("Updated Successfully");
        data.save();
        console.log("Updated data",data);
        });
        res.send("Updated Successfully");
  }
