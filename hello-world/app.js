'use strict';
var SwaggerExpress = require('swagger-express-mw');
// var express = require('express')
var app = require('express')();
var mongoose = require('mongoose');
var cors = require("cors");
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

mongoose.connect('mongodb://localhost/mongodbswagger');
mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'error in connection:'));
db.once('open', function(callback){
console.log('database connected')
});
app.use(cors());
SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware

app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://172.10.28.63:' + port + '/hello?name=Scott');
  }
});
