
import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms' 
import { UserService } from '../../../user.service';
import { Router } from '@angular/router';
import {ToastrModule, ToastrService} from 'ngx-toastr';
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
  })
export class SignupComponent implements OnInit {
  LoginForm:FormGroup;

    ToasterService
    constructor(private UserService : UserService, private router : Router, private toaster:ToastrService) { }

 ngOnInit() {
 
 this.LoginForm = new FormGroup ({

   "emailId": new FormControl(''),
   "password": new FormControl(''),
 })
   // saveEmployee(empForm: NgForm): void {
   //   console.log(empForm.value);
}
onSelect() {
 console.log(this.LoginForm.value);
 this.UserService.addLogin(this.LoginForm.value)
 .subscribe((res:any) => {
   console.log("response >>>>>>>>>>>>>>>>>>>>::", res);

   if(res.token)
   {
    this.router.navigate(["dashboard"])
   }
   else 
   {
   this.toaster.success("invalid user name and password")
   }
   console.log("response >>>>>>>>>>>>>>>>>>>>::", res);
  localStorage.setItem("token", res.token);
  localStorage.setItem("role", res.data.role);
  localStorage.setItem("userId", res.data._id);
  if(localStorage.getItem("token") !=null && localStorage.getItem("role") == "admin"){
  
  this.router.navigate(["/dashboard"]);
  }
  else{
    this.router.navigate(["/login"])
  };
 })
}
}
// onsubmit() {
//   this.submitted = true;

//   // stop here if form is invalid
//   if (this.loginForm.invalid) {
//     return;
//   }
//   alert('SUCCESS!! :-)')
//   console.log(this.loginForm.value);
//   this.auth.login(this.loginForm.value).subscribe((res: any) => {
//     //   res = res.data;
//     console.log('response ::', res);
//     var token = res.token;
//     localStorage.setItem("token", token);
//     if ( res.token == null) {
//       console.log("err");
//     } else {
//       this.router.navigate(['/dashboard'])
//     }
//   })
// }
// }


