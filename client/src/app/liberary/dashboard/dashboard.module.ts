import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardShowComponent } from './components/dashboard-show/dashboard-show.component';
import { ManageFilesComponent } from './components/manage-files/manage-files.component';
// import { ManageProductsComponent } from './components/manage-products/manage-products.component';
import { AddProductComponent } from './components/manage-products/add-product/add-product.component';
import { ListProductComponent } from './components/manage-products/list-product/list-product.component';
import { ProductOperatorComponent } from './components/manage-products/product-operator/product-operator.component';
import { UpdateProductComponent } from './components/manage-products/update-product/update-product.component';
import { ManageProductRoutingModule } from './components/manage-products/manage-product.router';
import { ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
// import { LogoutComponent } from './components/logout/logout.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    ManageProductRoutingModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  declarations: [DashboardShowComponent, ManageFilesComponent, AddProductComponent, ListProductComponent, ProductOperatorComponent, UpdateProductComponent]
})
export class DashboardModule { }
