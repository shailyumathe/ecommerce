import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AddProductService } from '../../../../../services/add-product.service';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  addProductForm:FormGroup;
  constructor(private fb:FormBuilder,private addProd:AddProductService) { }

  ngOnInit() {
    this.addProductForm=new FormGroup({
      "pname":new FormControl(''),
      "powner":new FormControl(''),
      "pcost":new FormControl(''),
      "scost":new FormControl(''),
      "add":new FormControl(''),
      "pnum":new FormControl(''),
      "stat":new FormControl(''),
      "date":new FormControl(''),
    })
  }
  onAdd(){
    this.addProd.addProduct(this.addProductForm.value)
    .subscribe((res:any)=>{
      res=res.data;
      console.log("Data",res)
    })
  }
}



// import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// import { AddProductService } from '../../../../../services/add-product.service';

// @Component({
//   selector: 'app-add-product',
//   templateUrl: './add-product.component.html',
//   styleUrls: ['./add-product.component.css']
// })
// export class AddProductComponent implements OnInit {
//   addProductForm:FormGroup;
//   constructor(private fb:FormBuilder,private addProd:AddProductService) { }

//   ngOnInit() {
//     this.addProductForm=new FormGroup({
//       "pname":new FormControl(''),
//       "powner":new FormControl(''),
//       "pcost":new FormControl(''),
//       "scost":new FormControl(''),
//       "add":new FormControl(''),
//       "pnum":new FormControl(''),
//       "stat":new FormControl(''),
//       "date":new FormControl(''),
//     })
//     // this.addProductForm= this.fb.group({  
//     //   pname:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],   
//     //   powner:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],   
//     //   pcost:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(3)])],   
//     //   scost:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(3)])],   
//     //   add:['',Validators.compose([Validators.required,Validators.maxLength(70),Validators.minLength(3)])],   
//     //   pnum:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(3)])],   
//     //   stat:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],   
//     //   date:['',Validators.required]
//     // })
//   }
//   onAdd(){
//     this.addProd.addProduct(this.addProductForm.value)
//     .subscribe((res:any)=>{
//       res=res.data;
//       console.log("Data",res)
//     })
//   }
// }
