// import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormControl } from '@angular/forms';
// import { Router, ActivatedRoute } from '@angular/router';
// import { UpdateProductService } from '../../../../../services/update-product.service';
// @Component({
//   selector: 'app-update-product',
//   templateUrl: './update-product.component.html',
//   styleUrls: ['./update-product.component.css']
// })
// export class UpdateProductComponent implements OnInit {
//   updateProductForm:FormGroup
//   root:Router
//   productlist:any
//   constructor(private updateproduct:UpdateProductService,private router:ActivatedRoute) { }

//   ngOnInit() {
//     this.updateProductForm=new FormGroup({
//       "newProductName":new FormControl(''),
//       "newProductCost":new FormControl(''),
//     })
//   }
//   onUpdate(){
//     this.router.paramMap.subscribe(params=>{
//       this.updateproduct.updateproduct(this.updateProductForm.value,params['id'].subscribe((res:any)=>{
//         res=res.data
//       }))
//     })
//   }
// }


import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UpdateProductService } from '../../../../../services/update-product.service';

@Component({
    selector: 'app-update-product',
    templateUrl: './update-product.component.html',
    styleUrls: ['./update-product.component.css']
  })
export class UpdateProductComponent implements OnInit {
  updateProductForm:FormGroup
  root:Router
  productlist:any
  constructor(private updateproduct:UpdateProductService,private router:ActivatedRoute) { }

  ngOnInit() {
    this.updateProductForm=new FormGroup({
      "newProductName":new FormControl(''),
      "newProductCost":new FormControl(''),
    })
  }
  onUpdate(){
    this.router.paramMap.subscribe(params=>{
      this.updateproduct.updateProduct(this.updateProductForm.value,params['id'].subscribe((res:any)=>{
        res=res.data
      }))
    })
  }
}
