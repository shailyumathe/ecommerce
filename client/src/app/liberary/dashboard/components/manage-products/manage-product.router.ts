import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
// import { ProductOperationComponent } from './product-operation/product-operation.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ListProductComponent } from './list-product/list-product.component';
import { ProductOperatorComponent } from './product-operator/product-operator.component';
import { UpdateProductComponent } from './update-product/update-product.component';
// import { UpdateProductDataComponent } from './update-product-data/update-product-data.component';
// import { ProductOperationComponent } from './components/manage-Product/product-operation/product-operation.component';

const routes : Routes = [
  {path : "manageproduct" , component : ProductOperatorComponent},
  {path : "addproduct" , component : AddProductComponent},
  {path : "listProduct" , component : ListProductComponent},
  {path : "updateProduct" , component : UpdateProductComponent},
]

@NgModule({
  imports: [
    CommonModule ,RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class ManageProductRoutingModule { }
