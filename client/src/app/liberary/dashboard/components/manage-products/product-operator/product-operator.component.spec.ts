import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOperatorComponent } from './product-operator.component';

describe('ProductOperatorComponent', () => {
  let component: ProductOperatorComponent;
  let fixture: ComponentFixture<ProductOperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductOperatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
