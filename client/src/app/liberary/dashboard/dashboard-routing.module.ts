import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductOperatorComponent } from './components/manage-products/product-operator/product-operator.component';
import { DashboardShowComponent } from './components/dashboard-show/dashboard-show.component';
import { AddProductComponent } from './components/manage-products/add-product/add-product.component';
import { ListProductComponent } from './components/manage-products/list-product/list-product.component';
import { UpdateProductComponent } from './components/manage-products/update-product/update-product.component';
import { ManageFilesComponent } from './components/manage-files/manage-files.component';

// const routes: Routes = [];
const routes : Routes = [
  // {path: "dashbord",component : DashboardShowComponent },
  {path : "" , component : ProductOperatorComponent},
  {path:'addproduct',component:AddProductComponent},
  {path:'listproduct',component:ListProductComponent},
  {path:'updateproduct',component:UpdateProductComponent},
  // {path:'logout',component:UpdateProductComponent},
  // {path:'manageusers',component:ManageUsersComponent},
  // {path:'managefiles',component:ManageFilesComponent}

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }


