
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 import { Observable} from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private http : HttpClient) { }
  addUser(userData : any):Observable<any>
  {
    console.log(">>>>>>>>>>>>>>data",userData);
    return this.http.post ("http://localhost:10010/register", userData);
  }

  addLogin(userData : any):Observable<any>
  {
    console.log(">>>>>>>>>>>>>>data",userData);
    return this.http.post ("http://localhost:10010/addLogin",userData );
  }
//   addList(userData : any):Observable<any>
//   {
//     console.log(">>>>>>>>>>>>>>data",userData);
//     return this.http.post ("http://localhost:3000/addproduct",userData );
//   }
getToken() {
  return localStorage.getItem("token")
}
isLoggednIn() {
  return this.getToken() !== null;
}

 }
