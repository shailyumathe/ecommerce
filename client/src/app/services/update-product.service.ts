import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UpdateProductService {

  constructor(private http:HttpClient) { }
  updateProduct(id,data):Observable<any>{
    console.log("Id is",id);
    // return this.http.put('http://localhost:10010/updateProduct/'+id,data);
    // return this.http.delete('http://localhost:10010/deleteproduct?id='+id);
    return this.http.put('http://localhost:10010/updateProduct?id='+id,data);
  }
}

