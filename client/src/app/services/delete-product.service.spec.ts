import { TestBed, inject } from '@angular/core/testing';

import { DeleteProductService } from './delete-product.service';

describe('DeleteProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteProductService]
    });
  });

  it('should be created', inject([DeleteProductService], (service: DeleteProductService) => {
    expect(service).toBeTruthy();
  }));
});
