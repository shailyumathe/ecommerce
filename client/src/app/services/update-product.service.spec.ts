import { TestBed, inject } from '@angular/core/testing';

import { UpdateProductService } from './update-product.service';

describe('UpdateProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdateProductService]
    });
  });

  it('should be created', inject([UpdateProductService], (service: UpdateProductService) => {
    expect(service).toBeTruthy();
  }));
});
