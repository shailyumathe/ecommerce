// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class ListProductService {

//   constructor() { }
// }
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListProductService {

  constructor(private http:HttpClient) { }
  productList():Observable<any>{
    return this.http.get('http://localhost:10010/productList')
  }
}
