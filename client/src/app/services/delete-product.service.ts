// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class DeleteProductService {

//   constructor() { }
// }

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteProductService {

  constructor(private http:HttpClient) { }
  deleteProduct(id):Observable<any>{
    console.log("Id is",id);
    //  return this.http.post("http://localhost:10010/deleteProduct/",{id:id})
    // return this.http.post('http://localhost:3000/deleteProduct/'+id, data);
    return this.http.delete('http://localhost:10010/deleteproduct?id='+id);

  }
}
