import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { MoreComponent } from './more/more.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FaqComponent, MoreComponent]
})
export class ServicesModule { }
