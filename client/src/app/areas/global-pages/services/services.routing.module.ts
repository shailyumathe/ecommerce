import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {Routes} from '@angular/router';
//import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
//import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { FormsModule, FormGroup }   from '@angular/forms';
import { FaqComponent } from './faq/faq.component';
import { MoreComponent } from './more/more.component';

const routes:Routes =[
   
  
{path:"", component: FaqComponent},
{path:"", component: MoreComponent}


]

@NgModule({
   
    imports: [
      CommonModule,FormsModule,
      FormGroup,
       RouterModule.forRoot(routes)],
    exports:[RouterModule],

      
  declarations: []
  })
  export class ServicesRoutingModule {}
  