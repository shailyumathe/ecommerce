import { GlobalPagesModule } from './global-pages.module';

describe('GlobalPagesModule', () => {
  let globalPagesModule: GlobalPagesModule;

  beforeEach(() => {
    globalPagesModule = new GlobalPagesModule();
  });

  it('should create an instance', () => {
    expect(globalPagesModule).toBeTruthy();
  });
});
