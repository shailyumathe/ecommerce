import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {Routes} from '@angular/router';


 const routes:Routes =[
    {path: "",loadChildren: "./dishes/dishes.module#DishesModule"},
    // {path: 'home' ,component: HomeComponent}
]

@NgModule({
   
    imports: [RouterModule.forChild(routes)],
    exports:[RouterModule]
      
  })
  export class UsersRouting {}
  
