import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRouting } from './user.routing';

@NgModule({
  imports: [
    CommonModule,UsersRouting
  ],
  declarations: []
})
export class UserModule { }
