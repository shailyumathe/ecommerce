import { Component, OnInit } from '@angular/core';
import { ListProductService } from '../services/list-product.service';
import { DeleteProductService } from '../services/delete-product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  productList:any;
  constructor(private productlist:ListProductService,private router:Router,private deleteRec:DeleteProductService) { }

  ngOnInit() {
    this.getList()
  }
  getList(){
    this.productlist.productList().subscribe(res=>{
      this.productList=res;
      console.log("List",this.productList)
    })
  }
  delete(id){
    this.deleteRec.deleteProduct(id).subscribe(res=>{
      console.log("deleted");
      this.router.navigate(['/listproduct'])
    })
}
}



// import { Component, OnInit } from '@angular/core';
// import { ListProductService } from '../../../../../services/list-product.service';
// import { DeleteProductService } from '../../../../../services/delete-product.service';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-list-product',
//   templateUrl: './list-product.component.html',
//   styleUrls: ['./list-product.component.css']
// })
// export class ListProductComponent implements OnInit {
//   productList:any;
//   constructor(private productlist:ListProductService,private router:Router,private deleteRec:DeleteProductService) { }

//   ngOnInit() {
//     this.getList()
//   }
//   getList(){
//     this.productlist.productList().subscribe(res=>{
//       this.productList=res;
//       console.log("List",this.productList)
//     })
//   }
//   delete(id){
//     this.deleteRec.deleteProduct(id).subscribe(res=>{
//       console.log("deleted");
//       this.router.navigate(['/listproduct'])
//     })
//     // this.productList.productList().subscribe(res=>{
//     //   this.productList=res;
//     //   console.log("List after deleting",this.productList)
//     // })
//   }
// }
