import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { AppRouting } from './app.routing';
import { HomeComponent } from './liberary/shared/component/home/home.component';
import { GlobalHeaderComponent } from './liberary/shared/component/global-header/global-header.component';
import { GlobalFooterComponent } from './liberary/shared/component/global-footer/global-footer.component';

//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
// import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material/list';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { LoginComponent } from './liberary/auth/login/login.component';
import {  ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { SignupComponent } from './liberary/auth/signup/signup.component';
import { UserService } from './user.service';
import { HttpClientModule } from '@angular/common/http';
// import { DashboardComponent } from './dashboard/dashboard.component';
import {ToastrModule} from 'ngx-toastr';
import { ListComponent } from './list/list.component';

// import { DashboardShowComponent } from './liberary/dashboard/components/dashboard-show/dashboard-show.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardModule } from './liberary/dashboard/dashboard.module';
//import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { LoginComponent } from './liberary/auth/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    HomeComponent,
    GlobalHeaderComponent,
    GlobalFooterComponent,
    LoginComponent,
    SignupComponent,
    // DashboardShowComponent,
    ListComponent,
    // DashboardComponent,
    
    
    
   
  ],
  imports: [
    BrowserModule,

    AppRouting,
    BrowserAnimationsModule,
    MatToolbarModule,
    // FlexLayoutModule
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    DashboardModule
    
   // MDBBootstrapModule.forRoot()
    
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
