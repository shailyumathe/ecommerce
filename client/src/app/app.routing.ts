import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {Routes} from '@angular/router';
import { HomeComponent } from './liberary/shared/component/home/home.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './liberary/auth/login/login.component';
import { SignupComponent } from './liberary/auth/signup/signup.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardShowComponent } from './liberary/dashboard/components/dashboard-show/dashboard-show.component';
import { AuthGuard } from './auth.guard';
// import { DashboardComponent } from './dashboard/dashboard.component';


const routes:Routes =[
   
    {
        path: "" ,
    component: HomeComponent
    },
    
    // {path: "",loadChildren: "./areas/user/user.module#UserModule"}
{
        path: "app" ,
    component: AppComponent,
    children:[
        {
            path:"",
            loadChildren: "./areas/user/user.module#UserModule"
        }
    ]
},
{
    path:"login",
// loadChildren: './library/auth/auth.module#AuthModule'
component:LoginComponent
},
{
    path:"signup",
// loadChildren: './library/auth/auth.module#AuthModule'
component:SignupComponent
},

{
path: "faq",
loadChildren: './areas/global-pages/services/services.module#ServicesModule'
},
// {
// path: "dashboard",
// component:DashboardComponent
{ path: 'dashboard',  component:DashboardShowComponent, canActivate: [AuthGuard] },

//  {path : "manageproduct" , loadChildren:"./library/dashboard/dashboard.module#DashboardModule"}
]

@NgModule({
   
    imports: [ RouterModule.forRoot(routes)],
    exports:[RouterModule ]
      
  })
  export class AppRouting {}
  